# device_allwinner

#### Description
​		This repository stores scripts for compiling and packing kernel images.




#### 获取代码流程
```
mkdir openharmony

cd openharmony

repo init -u ssh://git@gitee.com/openharmony-sig/manifest.git -b master --no-repo-verify -m devboard_awt507.xml

repo sync -c

repo forall -c 'git lfs pull'

./build/prebuilts_download.sh

```



#### 编译流程

```
cd ./device/allwinner/build

python3 patch.py               # 打鸿蒙系统相关补丁

./build.sh load                # 准备linux4.19环境

./build.sh config              #  选择相关板级配置
    #默认配置可一直按回车键
    Welcome to mkscript setup progress
    All available platform:

        0. linux
    Choice [linux]:
    All available linux_dev:
        0. bsp
    Choice [bsp]:
    All available kern_ver:
        0. linux-4.19
    Choice [linux-4.19]:
    All available ic:
        0. t507
    Choice [t507]:
    All available board:
        0. demo2.0_harmony
    Choice [demo2.0_harmony]:
    All available flash:
        0. default
        1. nor
    Choice [default]:

./build.sh                     # 编译linux4.19内核

cd ../../..                    # 回退到harmony根目录

./build.sh --product-name T507 # 编译T507 L2的system.img/vendor.img等

cd ./device/allwinner/build    # 重新进入allwinner/build目录

./build.sh pack                # 生成img镜像文件

cd ../../../out                # 进入out目录可看到生成的镜像文件

# 镜像文件为：t507_linux_demo2.0_harmony_uart0.img
```



#### 使用工具LiveSuit_V1.0.0.1烧录

1、连接好数据线和串口线；

2、打开烧录软件，选择好要烧录的镜像文件；

3、使用类MobaXterm等工具打开串口，串口可读写的前提按住键盘键"2",然后按这样机的reset键，软件自动进入烧写模式，等待烧写成功即可。