# device_allwinner

#### Description
​		This repository stores scripts for compiling and packing kernel images.




#### Get codes steps
```
mkdir openharmony

cd openharmony

repo init -u ssh://git@gitee.com/openharmony-sig/manifest.git -b master --no-repo-verify -m devboard_awt507.xml

repo sync -c

repo forall -c 'git lfs pull'

./build/prebuilts_download.sh
```




#### Compiling steps
```
cd ./device/allwinner/build

python3 patch.py               # Patching harmony system patches

./build.sh load                # Prepare the linux4.19 environment

./build.sh config              # Select board configuration
    Welcome to mkscript setup progress
    All available platform:
       0. linux
    Choice [linux]:
    All available linux_dev:
       0. bsp
    Choice [bsp]:
    All available kern_ver:
       0. linux-4.19
    Choice [linux-4.19]:
    All available ic:
       0. t507
    Choice [t507]:
    All available board:
       0. demo2.0_harmony
    Choice [demo2.0_harmony]:
    All available flash:
       0. default
       1. nor
    Choice [default]:

./build.sh                     # Compile the Linux4.19 kernel

cd ../../..

./build.sh --product-name T507 # Compile T507 L2 system.img/vendor.img, etc

cd ./device/allwinner/build

./build.sh pack                # pack image file

cd ../../../out

# The image file is：t507_linux_demo2.0_harmony_uart0.img
```


#### Burn with LiveSuit_V1.0.0.1

```
1. Connect the data cable and serial cable;

2, open the burning software, select a good image file to burn;

3. Use tools such as MobaXterm to open the serial port. Press and hold down the keyboard key "2" before the serial port can be read and written, and then press the reset button of the machine.
```



