import os
import patch
import sys
from subprocess import Popen, PIPE

curent_path = os.getcwd()
root_path = os.path.dirname(os.path.dirname(os.path.dirname(curent_path)))
allwinner_path = os.path.join(root_path, 'device', 'allwinner')
t507_path = os.path.join(allwinner_path, 't507')
patch_path = os.path.join(t507_path, 'patches', 'harmony')
source_path = os.path.join(t507_path, 'sources')
code_source_path = os.path.join(source_path, 'code')
file_source_path = os.path.join(source_path, 'file')

def clean_code(patch_set):
  print("----------- clean code ----------")
  for git_path in patch_set.keys():
    print(git_path)
    p = Popen("git restore .", shell=True, stdout=PIPE, cwd=git_path)
    p.wait()
    p = Popen("git clean -fd", shell=True, stdout=PIPE, cwd=git_path)
    p.wait()

def patch():
  print("----------patch code to harmony--------")
  patch_set = get_file_set(patch_path)
  if not patch_set:
    print("---- [fasle] no patch to do ----")
    sys.exit(1)
  #检查每个补丁路径是否正确
  if not check_gits_exit(patch_set.keys()):
    return False
  clean_code(patch_set)
  patch_code_to_harmony(patch_set)
  print("---- [success] patch code to harmony -----")

def patch_code_to_harmony(patch_set):
  for git_path in patch_set.keys():
    if not patch_set[git_path]:
      continue
    patch_path = patch_set[git_path]
    p = Popen(f"git apply {patch_path}/*.patch ",shell=True, stdout=PIPE, cwd=git_path)
    p.wait()

def push_source():
  print("----------push source to harmony-------")
  source_set = get_file_set(code_source_path)
  if not source_set:
    print("---- [false] no resource ----")
    return False
  push_source_to_harmony(source_set)

def push_source_to_harmony(source_set):
  for git_path in source_set.keys():
    if not source_set[git_path]:
      continue
    source_path = source_set[git_path]
    p = Popen(f"cp -a {source_path}/* {git_path}/", shell=True, stdout=PIPE)
    p.wait()
    print("---- [success] push source to harmony ----")

def check_gits_exit(git_paths: list):
  for path in git_paths:
    git_path = os.path.join(path, ".git")
    if not os.path.isdir(git_path):
      print(f"---- [false] not found {git_path} ----")
      return False
  return True

def get_file_set(path):
  file_set = {}
  if not os.path.isdir(path):
    print(f"not fount {path}, please check")
    sys.exit(1)
  git_dir_list = []
  for root, dirs, files in os.walk(path):
    for dir in dirs:
      dir_path = os.path.join(root, dir)
      if os.path.isdir(dir_path):
        git_path = os.path.join(root_path, str(dir).replace("-", "/"))
        file_set[git_path] = dir_path
    break
  return file_set

def copy_new_file(src_path, dest_path):
  if not os.path.isdir(dest_path):
    print(f"[error] not found {dest_path}, please cheak!")
    return false
  if not os.path.exists(src_path):
    print(f"[error] not fount {src_path}, please check!")
    return false
  p = Popen(f"cp -a {src_path} {dest_path}/", shell=True, stdout=PIPE)
  p.wait()

def copy_file():
  src_camera_t507_gni = os.path.join(file_source_path, "camera", "camera.T507.gni")
  dest_camera_t507_gni = os.path.join(root_path, 'drivers/peripheral/camera/hal/adapter/chipset/gni')
  copy_new_file(src_camera_t507_gni, dest_camera_t507_gni)

  src_device_t507_json = os.path.join(file_source_path, 'productdefine', 'device', 'T507.json')
  dest_device_t507_json = os.path.join(root_path, 'productdefine/common/device')
  copy_new_file(src_device_t507_json, dest_device_t507_json)

  src_product_t507_json = os.path.join(file_source_path, 'productdefine', 'product', 'T507.json')
  dest_product_t507_json = os.path.join(root_path, 'productdefine/common/products')
  copy_new_file(src_product_t507_json, dest_product_t507_json)
  print("---- [success] copy file to harmony ----")


if __name__ == '__main__':
  print("start to patch")
  patch()
  push_source()
  copy_file()
